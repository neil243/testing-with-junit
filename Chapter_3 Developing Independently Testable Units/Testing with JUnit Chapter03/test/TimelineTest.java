import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.AfterEach;

import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

import static java.util.Collections.emptyList;

public class TimelineTest
{
	private static final FakeItem FIRST_ITEM = new FakeItem(10);
	private static final FakeItem SECOND_ITEM = new FakeItem(20);
	private static final FakeItem THIRD_ITEM = new FakeItem(30);
	
	private static final int NEW_FETCH_COUNT = 
			new Timeline( new ItemProviderDummy() ).getFetchCount() + 1;
	
	private Timeline timeline;
	private ItemProvider itemProvider;
	private SessionStorage sessionStorage;
	
	@BeforeEach
	public void setUp()
	{
		itemProvider = new ItemProviderStub();
		sessionStorage = mock(SessionStorage.class);
		timeline = new Timeline(itemProvider, sessionStorage);
	}
	
	@AfterEach
	public void tearDown()
	{
		timeline.dispose();
	}
	
	/*
	@Test // 1st version using spy
	public void fetchFirstItems()
	{
		((ItemProviderStub)itemProvider).addItems(FIRST_ITEM, SECOND_ITEM);
		timeline.setFetchCount(1);
		
		timeline.fetchItems();
		List<Item> actual = timeline.getItems();
		
		assertEquals(1, ( (SessionStorageSpy)sessionStorage).getLog().size() );
		assertSame(SECOND_ITEM, ((SessionStorageSpy)sessionStorage).getLog().get(0));
		assertArrayEquals(new Item[] { SECOND_ITEM }, actual.toArray( new Item[ 1 ] ) );
	}*/
	
	/*@Test // 2nd version using hand-crafted mock
	public void fetchFirstItems()
	{
		( (ItemProviderStub)itemProvider ).addItems( FIRST_ITEM, SECOND_ITEM );
		timeline.setFetchCount(1);
		((SessionStorageMock)sessionStorage).setExpectedItem(SECOND_ITEM);
		
		timeline.fetchItems();
		List<Item> actual = timeline.getItems();
		
		((SessionStorageMock)sessionStorage).verify();
		assertArrayEquals(new Item[] {SECOND_ITEM}, actual.toArray(new Item[ 1 ]));
		
	}*/
	
	@Test
	public void fetchFirstItems()
	{
		(( ItemProviderStub ) itemProvider ).addItems( FIRST_ITEM, SECOND_ITEM );
		timeline.setFetchCount( 1 );
		
		timeline.fetchItems();
		List<Item> actual = timeline.getItems();
		
		verify( sessionStorage ).storeTop( SECOND_ITEM );
		assertEquals(1, actual.size());
		assertSame(SECOND_ITEM, actual.get(0));
	}
	
	/*@Test // third version using Mockito
	public void fecthFirstItemsWithTopItemToRecover()
	{
		((ItemProviderStub) itemProvider).addItems( FIRST_ITEM, SECOND_ITEM, THIRD_ITEM );
		when( sessionStorage.readTop() ).thenReturn( SECOND_ITEM );
		timeline.setFetchCount( 1 );
		
		timeline.fetchItems();
		List<Item> actual = timeline.getItems();
		
		assertEquals(1, actual.size());
		assertSame(SECOND_ITEM, actual.get(0));
		verify( sessionStorage ).storeTop( SECOND_ITEM );
	}*/
	
	/*///////////@Test
	public void fetchFirstItemsWithTopItemToRecover()
	{
		// Here we mock both the item provider and the sessionStorage
		when( sessionStorage.readTop() ).thenReturn( SECOND_ITEM );
		when( itemProvider.fetchItems(SECOND_ITEM, 0) ).thenReturn(emptyList());
		timeline.setFetchCount(1);
		
		
		timeline.fetchItems();
		List<Item> actual = timeline.getItems();
		
		assertEquals(actual.size(), 1);
		assertSame(SECOND_ITEM, actual.get(0));
		verify( sessionStorage ).storeTop( SECOND_ITEM );
		
	}/*****
	
	// Test commented out since it will fail because of SessionStorageMock
	// implementation. We are fetching twice here and second fetch causes
	// a failed assertion in SessionStorageMock since storeTopDone field
	// is true at that point. (hint: assertFalse( true );
	/*@Test
	public void fetchItems()
	{
		// (2) exercise
		((ItemProviderStub)itemProvider).addItems(FIRST_ITEM, SECOND_ITEM, THIRD_ITEM);
		timeline.setFetchCount(1);
		timeline.fetchItems();
		
		timeline.fetchItems();
		List<Item> actual = timeline.getItems();
		
		// (3) verify
		assertArrayEquals(new Item[] {THIRD_ITEM, SECOND_ITEM}, 
				actual.toArray(new Item[2]));
	}*/
	
	@Test
	public void setFecthCount() 
	{
		// (1) setup -- actually done implicitly
		
		// (2) exercise
		timeline.setFetchCount(NEW_FETCH_COUNT);
		
		// (3) verify
		assertEquals(NEW_FETCH_COUNT, timeline.getFetchCount());
	}
	
	@Test
	public void initialState()
	{
		// (1) setup -- actually done implicitly
		
		// (2) exercise
		
		// (3) verify
		assertTrue(timeline.getFetchCount() > 0);
	}
	
	@Test
	public void setFetchCountExceedsLowerBound()
	{
		// (1) setup
		int originalFetchCount = timeline.getFetchCount();
		
		// (2) exercise
		timeline.setFetchCount(Timeline.FETCH_COUNT_LOWER_BOUND - 1);
		
		// (3) verify
		assertEquals(originalFetchCount, timeline.getFetchCount());
	}
	
	@Test
	public void setFetchCountExceedsUpperBound()
	{
		// (1) setup
		int originalFetchCount = timeline.getFetchCount();
		
		// (2) exercise
		timeline.setFetchCount(Timeline.FETCH_COUNT_UPPER_BOUND + 1);
		
		// (3) verify
		assertEquals(originalFetchCount, timeline.getFetchCount());
	}
}