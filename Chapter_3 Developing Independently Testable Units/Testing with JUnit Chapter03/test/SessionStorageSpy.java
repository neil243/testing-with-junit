import java.util.List;
import java.util.ArrayList;

public class SessionStorageSpy implements SessionStorage
{
	private final List<Item> log;
	
	public SessionStorageSpy()
	{
		log = new ArrayList<>();
	}
	
	@Override
	public void storeTop(Item top)
	{
		log.add(top);
	}

	@Override
	public Item readTop()
	{
		return null;
	}
	
	public List<Item> getLog()
	{
		return log;
	}
}