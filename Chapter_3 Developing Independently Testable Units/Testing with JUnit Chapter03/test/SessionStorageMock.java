import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;

public class SessionStorageMock implements SessionStorage
{
	private boolean storeTopDone;
	private Item expectedItem;
	
	@Override
	public void storeTop(Item top)
	{
		assertFalse(storeTopDone);
		assertSame(expectedItem, top);
		storeTopDone = true;
	}
	
	void setExpectedItem(Item expectedItem)
	{
		this.expectedItem = expectedItem;
	}
	
	void verify()
	{
		assertTrue(storeTopDone);
	}

	@Override
	public Item readTop()
	{
		return null;
	}
}
