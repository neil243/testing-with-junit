import java.util.*;

public class ItemProviderDummy implements ItemProvider
{
	private static String MESSAGE = "Dummy method must never be called";
	
	public List<Item> fetchItems(Item ancestor, int fetchCount)
	{
		throw new UnsupportedOperationException( MESSAGE );
	}
	
	public int getNewCount(Item predecessor)
	{
		throw new UnsupportedOperationException( MESSAGE );
	}
	
	public List<Item> fetchNew(Item predecessor)
	{
		throw new UnsupportedOperationException( MESSAGE );
	}
}
