
public class FakeItem implements Item
{
	private long timeStamp;
	
	public FakeItem(long timeStamp)
	{
		this.timeStamp = timeStamp;
	}
	
	@Override
	public long getTimeStamp()
	{
		return timeStamp;
	}

}
