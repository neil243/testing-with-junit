package testingWithJUnitBook.TestingWithJUnitChapter06;

public interface SessionStorage
{
	void store(Memento memento);
	Memento read();
}
