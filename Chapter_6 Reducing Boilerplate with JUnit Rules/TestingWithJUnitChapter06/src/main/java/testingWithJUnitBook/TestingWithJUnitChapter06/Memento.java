package testingWithJUnitBook.TestingWithJUnitChapter06;

public class Memento
{
	private String content;
	
	public Memento(String content)
	{
		this.content = content;
	}
	
	@Override
	public String toString()
	{
		return content;
	}
}
