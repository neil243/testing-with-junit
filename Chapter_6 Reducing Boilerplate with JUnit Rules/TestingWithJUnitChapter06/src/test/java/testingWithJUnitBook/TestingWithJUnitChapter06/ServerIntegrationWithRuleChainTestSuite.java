package testingWithJUnitBook.TestingWithJUnitChapter06;

import org.junit.runner.RunWith;
import org.junit.extensions.cpsuite.ClasspathSuite;
import org.junit.extensions.cpsuite.ClasspathSuite.ClassnameFilters;

import org.junit.ClassRule;
import org.junit.rules.TestRule;
import org.junit.rules.RuleChain;

@RunWith( ClasspathSuite.class )
@ClassnameFilters( { ".*ServerTest" } )
public class ServerIntegrationWithRuleChainTestSuite
{
	@ClassRule
	public static TestRule chain = RuleChain
		.outerRule( new ServerRule( 4711 ) )
		.around( new MyRule() );
}
