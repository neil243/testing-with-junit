package testingWithJUnitBook.TestingWithJUnitChapter06;

import static org.junit.Assert.*;

import org.junit.Test;

import org.junit.Rule;

import org.junit.contrib.java.lang.system.TextFromStandardInputStream;

import java.util.Scanner;
import java.io.InputStream;

public class ProvideSystemInputExample
{
	private static final String INPUT = "input";
	
	@Rule
	public final TextFromStandardInputStream systemInRule = 
		TextFromStandardInputStream.emptyStandardInputStream();
	
	@Test
	public void stubInput()
	{
		systemInRule.provideLines( INPUT );
		
		assertEquals( INPUT, readLine( System.in ));
	}
	
	@SuppressWarnings("resource")
	private String readLine(InputStream inputstream)
	{
		return new Scanner(inputstream).nextLine();
	}
}
