package testingWithJUnitBook.TestingWithJUnitChapter06;

import static org.junit.Assert.*;

import org.junit.Test;

import org.junit.contrib.java.lang.system.ClearSystemProperties;

import org.junit.Rule;

public class ClearPropertiesExample
{
	private static final String JAVA_IO_TMPDIR = "java.io.tmpdir";
	
	@Rule
	public final ClearSystemProperties clearTempDirRule = 
				new ClearSystemProperties( JAVA_IO_TMPDIR );
	
	@Test
	public void checkTempDir()
	{
		assertNull(System.getProperty( JAVA_IO_TMPDIR ));
	}
}
