package testingWithJUnitBook.TestingWithJUnitChapter06;

import org.junit.rules.TestRule;
import org.junit.runners.model.Statement;
import org.junit.runner.Description;

public class MyRule implements TestRule
{
	public Statement apply( Statement base, Description description )
	{
		return new MyStatement( base );
	}
	
	class MyStatement extends Statement
	{
		private final Statement base;
		
		MyStatement( Statement base )
		{
			this.base = base;
		}
		
		public void evaluate() throws Throwable
		{
			System.out.println( "before" );
			try
			{
				base.evaluate();
			}
			finally
			{
				System.out.println( "after" );
			}
		}
	}
}
