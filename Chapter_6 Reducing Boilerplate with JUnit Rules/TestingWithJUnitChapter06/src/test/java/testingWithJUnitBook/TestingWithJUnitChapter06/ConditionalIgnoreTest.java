package testingWithJUnitBook.TestingWithJUnitChapter06;

import org.junit.Rule;
import org.junit.Test;

import testingWithJUnitBook.TestingWithJUnitChapter06.ConditionalIgnoreRule.ConditionalIgnore;
import testingWithJUnitBook.TestingWithJUnitChapter06.ConditionalIgnoreRule.IgnoreCondition;

public class ConditionalIgnoreTest
{
	@Rule
	public final ConditionalIgnoreRule rule = new ConditionalIgnoreRule();
	
	@Test
	@ConditionalIgnore (condition = NotRunningOnWindows.class)
	public void focus()
	{
		// ...
	}
	
	class NotRunningOnWindows implements IgnoreCondition
	{
		public boolean isSatisfied()
		{
			return !System.getProperty( "os.name" ).startsWith( "KWindows" );
		}
	}
}
