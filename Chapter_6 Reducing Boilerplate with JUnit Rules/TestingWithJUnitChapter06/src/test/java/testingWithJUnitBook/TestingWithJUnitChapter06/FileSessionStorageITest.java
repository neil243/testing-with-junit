package testingWithJUnitBook.TestingWithJUnitChapter06;

import static testingWithJUnitBook.TestingWithJUnitChapter06.ThrowableCaptor.thrownBy;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.Before;

import org.junit.Rule;
import org.junit.rules.TemporaryFolder;

import java.io.File;

import java.io.IOException;

public class FileSessionStorageITest
{
	private static final String CONTENT = "content";
	
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();
	
	private FileSessionStorage storage;
	private File storageLocation;
	
	@Before
	public void setUp() throws IOException
	{
		storageLocation = temporaryFolder.newFile();
		storage = new FileSessionStorage(storageLocation);
	}
	
	@Test
	public void store() throws IOException
	{
		Memento memento = new Memento( CONTENT );
		
		storage.store( memento );
		
		assertEquals( CONTENT ,  readStoredContent() );
	}
	
	@Test
	public void storeIfStorageLocationDoesNotExist()
	{
		storageLocation.delete();
		
		Throwable actual = thrownBy( () -> storage.store( new Memento(CONTENT) ) );
		
		assertTrue( actual instanceof IllegalStateException );
		assertTrue( actual.getCause() instanceof IOException );
	}
	
	@Test
	public void read() throws IOException
	{
		writeContentToStore(CONTENT);
		
		Memento memento = storage.read();
		
		assertEquals( CONTENT, memento.toString() );
	}
	
	@Test
	public void readIfStorageLocationDoesNotExist()
	{
		storageLocation.delete();
		
		Throwable actual = thrownBy( () -> storage.read() );
		
		assertTrue( actual instanceof IllegalStateException );
		assertTrue( actual.getCause() instanceof IOException );
	}
	
	private String readStoredContent() throws IOException
	{
		byte[] bytes = Files.readAllBytes( storageLocation.toPath() );
		return new String( bytes, StandardCharsets.UTF_8 );
	}
	
	private void writeContentToStore( String content ) throws IOException
	{
		byte[] bytes = content.getBytes( StandardCharsets.UTF_8 );
		Files.write( storageLocation.toPath(), bytes );
	}
}
