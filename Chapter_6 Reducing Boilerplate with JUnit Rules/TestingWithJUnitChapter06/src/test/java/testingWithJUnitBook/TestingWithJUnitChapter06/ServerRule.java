package testingWithJUnitBook.TestingWithJUnitChapter06;

import org.junit.rules.ExternalResource;

public class ServerRule extends ExternalResource
{
	private final int port;
	
	public ServerRule(int port)
	{
		this.port = port;
	}
	
	protected void before()
	{
		System.out.println( "start server on port: " + port);
	}
	
	protected void after()
	{
		System.out.println( "stop server on port: " + port);
	}
}
