package testingWithJUnitBook.TestingWithJUnitChapter06;

import org.junit.runner.RunWith;
import org.junit.extensions.cpsuite.ClasspathSuite;
import org.junit.extensions.cpsuite.ClasspathSuite.ClassnameFilters;
import org.junit.ClassRule;

@RunWith(ClasspathSuite.class)
@ClassnameFilters({ ".*ServerTest" })
public class ServerIntegrationTestSuite
{
	@ClassRule
	public static ServerRule serverRule = new ServerRule( 4711 );
}
