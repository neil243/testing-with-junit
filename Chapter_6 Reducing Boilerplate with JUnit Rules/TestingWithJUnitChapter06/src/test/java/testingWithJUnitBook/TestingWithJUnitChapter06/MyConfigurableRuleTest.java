package testingWithJUnitBook.TestingWithJUnitChapter06;

import org.junit.Test;

import org.junit.Rule;

public class MyConfigurableRuleTest
{
	@Rule
	public MyConfigurableRule myConfigurableRule = new MyConfigurableRule();
	
	@Test
	@MyRuleConfiguration( "myConfigurationValue" )
	public void testRun()
	{
		System.out.println( "during" );
	}
}
