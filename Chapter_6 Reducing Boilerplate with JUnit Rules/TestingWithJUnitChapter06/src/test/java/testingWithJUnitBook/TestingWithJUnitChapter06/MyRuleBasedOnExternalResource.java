package testingWithJUnitBook.TestingWithJUnitChapter06;

import org.junit.rules.ExternalResource;

public class MyRuleBasedOnExternalResource extends ExternalResource
{
	public void before()
	{
		System.out.println( "before" );
	}
	
	public void after()
	{
		System.out.println( "after" );
	}
}
