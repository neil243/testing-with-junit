package testingWithJUnitBook.TestingWithJUnitChapter06;

import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

public class MyConfigurableRule implements TestRule
{
	public Statement apply( Statement base, Description description )
	{
		return new MyConfigurableStatement(base, description);
	}
	
	private class MyConfigurableStatement extends Statement
	{
		private final Statement base;
		private final Description description;
		
		MyConfigurableStatement( Statement base, Description description )
		{
			this.base = base;
			this.description = description;
		}
		
		public void evaluate() throws Throwable
		{
			String configuration = getConfiguration();
			System.out.println( "before [" + configuration + "]" );
			try
			{
				base.evaluate();
			}
			finally
			{
				System.out.println( "after [" + configuration + "]");
			}
		}
		
		private String getConfiguration()
		{
			return description.getAnnotation( MyRuleConfiguration.class ).value();
		}
	}
}
