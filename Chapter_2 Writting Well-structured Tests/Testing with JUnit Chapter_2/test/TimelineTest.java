import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.AfterEach;

public class TimelineTest
{
	private static final int NEW_FETCH_COUNT = 
			new Timeline().getFetchCount() + 1;
	
	private Timeline timeline;
	
	@BeforeEach
	public void setUp()
	{
		timeline = new Timeline();
	}
	
	@Test
	public void setFecthCount() 
	{
		// (1) setup -- actually done implicitly
		
		// (2) exercise
		timeline.setFetchCount(NEW_FETCH_COUNT);
		
		// (3) verify
		assertEquals(NEW_FETCH_COUNT, timeline.getFetchCount());
	}
	
	@Test
	public void initialState()
	{
		// (1) setup -- actually done implicitly
		
		// (2) exercise
		
		// (3) verify
		assertTrue(timeline.getFetchCount() > 0);
	}
	
	@Test
	public void setFetchCountExceedsLowerBound()
	{
		// (1) setup
		int originalFetchCount = timeline.getFetchCount();
		
		// (2) exercise
		timeline.setFetchCount(Timeline.FETCH_COUNT_LOWER_BOUND - 1);
		
		// (3) verify
		assertEquals(originalFetchCount, timeline.getFetchCount());
	}
	
	@Test
	public void setFetchCountExceedsUpperBound()
	{
		// (1) setup
		int originalFetchCount = timeline.getFetchCount();
		
		// (2) exercise
		timeline.setFetchCount(Timeline.FETCH_COUNT_UPPER_BOUND + 1);
		
		// (3) verify
		assertEquals(originalFetchCount, timeline.getFetchCount());
	}
	
	@AfterEach
	public void tearDown()
	{
		timeline.dispose();
	}

}
