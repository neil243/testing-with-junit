package UnitTesting.TestingWithJUnitChapter04;

import static UnitTesting.TestingWithJUnitChapter04.ThrowableCaptor.thrownBy;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.any;

import java.io.IOException;

class Listing_5_Collaborator_TimelineTest
{	
	private Timeline timeline;
	private ItemProvider itemProvider;
	private SessionStorage sessionStorage;
	
	@BeforeEach
	public void setUp()
	{
		sessionStorage = mock( SessionStorage.class );
		itemProvider = mock( ItemProvider.class );
		timeline = new Timeline(itemProvider, sessionStorage);
	}
	
	@Test
	public void fetchItemWithIOExceptionOnStoreTop() throws IOException
	{
		IOException cause = new IOException();
		doThrow( cause ).when( sessionStorage ).storeTop( any(Item.class) );
		doThrow( cause ).when( sessionStorage ).storeTop( null );
		
		Throwable actual = thrownBy( () -> timeline.fetchItems() );
		
		assertNotNull( actual );
		assertTrue( actual instanceof IllegalStateException);
		assertSame(cause, actual.getCause());
		assertEquals( Timeline.ERROR_STORE_TOP, actual.getMessage() );		
	}
	
	@Test
	public void fetchItemWithRuntimeExceptionOnStoreTop() throws IOException
	{
		RuntimeException cause = new RuntimeException();
		doThrow( cause ).when( sessionStorage ).storeTop( any( Item.class ) );
		doThrow( cause ).when( sessionStorage ).storeTop( null );
		
		Throwable actual = thrownBy( () -> timeline.fetchItems() );
		
		assertNotNull( actual );
		assertSame(cause, actual);
	}
}
