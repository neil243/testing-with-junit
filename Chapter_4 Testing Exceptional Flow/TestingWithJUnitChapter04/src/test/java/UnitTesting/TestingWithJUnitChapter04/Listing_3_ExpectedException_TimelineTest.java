// Note: This test case does not work as expected due
// to the following deprecated libraries:
// org.hamcrest.junit.ExpectedException and
// org.junit.rules.ExpectedException;

package UnitTesting.TestingWithJUnitChapter04;

import static java.lang.String.valueOf;

import static UnitTesting.TestingWithJUnitChapter04.Timeline.FETCH_COUNT_LOWER_BOUND;
import static UnitTesting.TestingWithJUnitChapter04.Timeline.FETCH_COUNT_UPPER_BOUND;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import org.junit.Rule;
import org.hamcrest.junit.ExpectedException;

import static org.mockito.Mockito.mock;

class Listing_3_ExpectedException_TimelineTest
{
	@Rule
	ExpectedException thrown = ExpectedException.none();
	
	private Timeline timeline;
	
	@BeforeEach
	public void setUp()
	{
		timeline = new Timeline( mock(ItemProvider.class), mock(SessionStorage.class) );
	}
	
	@Test
	public void setFetchCountExceedsUpperBound()
	{
		int tooLargeValue = Timeline.FETCH_COUNT_UPPER_BOUND + 1;
		thrown.expect( IllegalArgumentException.class );
		thrown.expectMessage( valueOf( tooLargeValue ) );
		
		timeline.setFetchCount(tooLargeValue);
	}
	
	@Test
	public void setFetchCountExceedsLowerBound()
	{
		int tooSmallValue = Timeline.FETCH_COUNT_LOWER_BOUND - 1;
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage( valueOf( tooSmallValue ) );
		
		timeline.setFetchCount(tooSmallValue);
	}
	
	@Test
	public void constructWithNullAsItemProvider()
	{
		assertThrows(IllegalArgumentException.class, 
				() -> new Timeline( null, mock( SessionStorage.class ) ) );
	}
	
	@Test
	public void constructWithNullAsSessionStorage()
	{
		assertThrows(IllegalArgumentException.class, 
				() -> new Timeline( mock( ItemProvider.class), null ) );
	}
}