package UnitTesting.TestingWithJUnitChapter04;

import static UnitTesting.TestingWithJUnitChapter04.Timeline.FETCH_COUNT_LOWER_BOUND;
import static UnitTesting.TestingWithJUnitChapter04.Timeline.FETCH_COUNT_UPPER_BOUND;
import static UnitTesting.TestingWithJUnitChapter04.Timeline.ERROR_EXCEEDS_LOWER_BOUND;
import static UnitTesting.TestingWithJUnitChapter04.Timeline.ERROR_EXCEEDS_UPPER_BOUND;

import static UnitTesting.TestingWithJUnitChapter04.ThrowableCaptor.*;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.mockito.Mockito.mock;

import static java.lang.String.valueOf;
import static java.lang.String.format;

class Listing_4_Closure_TimelineTest
{
	private Timeline timeline;
	
	@BeforeEach
	public void setUp()
	{
		timeline = new Timeline( mock(ItemProvider.class), mock(SessionStorage.class) );
	}
	
	@Test
	public void setFetchCountExceedsUpperBound()
	{
		int tooLarge = FETCH_COUNT_UPPER_BOUND + 1;
		
		Throwable actual = thrownBy( () -> timeline.setFetchCount(tooLarge) );
		
		assertNotNull( actual );
		assertTrue( actual instanceof IllegalArgumentException );
		assertTrue( actual.getMessage().contains( valueOf(tooLarge) ) );
		assertEquals( format(ERROR_EXCEEDS_UPPER_BOUND, tooLarge), actual.getMessage());
	}
	
	@Test
	public void setFetchCountExceedsLowerBound()
	{
		int tooSmall = FETCH_COUNT_LOWER_BOUND - 1;
		
		Throwable actual = thrownBy( () -> timeline.setFetchCount( tooSmall ));
		
		assertNotNull(actual);
		assertTrue(actual instanceof IllegalArgumentException);
		assertTrue( actual.getMessage().contains( valueOf( tooSmall ) ) );
		assertEquals( format( ERROR_EXCEEDS_LOWER_BOUND, tooSmall ), actual.getMessage());
	}
	
	@Test
	public void constructWithNullAsItemProvider()
	{
		assertThrows(IllegalArgumentException.class, 
				() -> new Timeline(null, mock( SessionStorage.class )));
	}
	
	@Test
	public void constructWithNullAsSessionStorage()
	{
		assertThrows(IllegalArgumentException.class,
				() -> new Timeline( mock( ItemProvider.class ), null ));
	}
}