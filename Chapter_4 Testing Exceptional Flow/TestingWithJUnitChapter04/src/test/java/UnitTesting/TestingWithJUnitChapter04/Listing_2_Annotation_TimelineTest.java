package UnitTesting.TestingWithJUnitChapter04;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;


class Listing_2_Annotation_TimelineTest
{
	
	@Test
	public void constructWithNullAsItemProvider()
	{
		assertThrows(IllegalArgumentException.class, 
				() -> new Timeline( null, mock(SessionStorage.class ) ) );
	}
	
	@Test
	public void constructWithNullAsSessionStorage()
	{
		assertThrows(IllegalArgumentException.class, 
				() -> new Timeline( mock(ItemProvider.class), null) );
	}
	
	@Test
	public void setFetchCountExceedsLowerBound()
	{
		Timeline timeline = createTimeline();
		
		assertThrows(IllegalArgumentException.class, 
				() -> timeline.setFetchCount(Timeline.FETCH_COUNT_LOWER_BOUND - 1));
	}
	
	@Test
	public void setFetchCountExceedsUpperBound()
	{
		Timeline timeline = createTimeline();
		
		assertThrows(IllegalArgumentException.class, 
				() -> timeline.setFetchCount(Timeline.FETCH_COUNT_UPPER_BOUND + 1) );
	}
	
	public Timeline createTimeline()
	{
		return new Timeline( mock( ItemProvider.class ), 
							 mock( SessionStorage.class ) );
	}
}
