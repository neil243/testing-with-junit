package UnitTesting.TestingWithJUnitChapter04;

public class FakeItem implements Item
{
	private final long timeStamp;
	  
	FakeItem( long timeStamp )
	{
		this.timeStamp = timeStamp;
	}
	
	public long getTimeStamp()
	{
	    return timeStamp;
	}

  
	public String toString()
	{
		return "FakeItem [timeStamp=" + timeStamp + "]";
	}
}