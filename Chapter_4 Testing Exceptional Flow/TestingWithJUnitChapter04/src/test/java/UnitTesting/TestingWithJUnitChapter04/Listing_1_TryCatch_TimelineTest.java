package UnitTesting.TestingWithJUnitChapter04;

import static java.lang.String.format;
import static java.lang.String.valueOf;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.Assert.fail;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.mockito.Mockito.mock;

public class Listing_1_TryCatch_TimelineTest
{	
	private SessionStorage sessionStorage;
	private ItemProviderStub itemProvider;
	private Timeline timeline;
	
	@BeforeEach
	public void setUp()
	{
		itemProvider = new ItemProviderStub();
		sessionStorage = mock(SessionStorage.class);
		timeline = new Timeline( itemProvider, sessionStorage );
	}
	
	@Test
	public void setFetchCountExceedsLowerBound()
	{
		int tooSmall = Timeline.FETCH_COUNT_LOWER_BOUND - 1;
		
		try
		{
			timeline.setFetchCount(tooSmall);
			fail();			
		}
		catch(IllegalArgumentException actual)
		{
			String message = actual.getMessage();
			String expected = format(Timeline.ERROR_EXCEEDS_LOWER_BOUND, tooSmall);
			
			assertEquals(expected, message);
			assertTrue(message.contains( valueOf(tooSmall) ));
		}
	}
	
	@Test
	public void setFetchCountExceedsUpperBound()
	{
		int tooLarge = Timeline.FETCH_COUNT_UPPER_BOUND + 1;
		
		try
		{
			timeline.setFetchCount(tooLarge);
		}
		catch(IllegalArgumentException actual)
		{
			String message = actual.getMessage();
			String expected = format( Timeline.ERROR_EXCEEDS_UPPER_BOUND, tooLarge );
			
			assertEquals( expected, message );
			assertTrue( message.contains( valueOf( tooLarge ) ) );
		}
	}
}
