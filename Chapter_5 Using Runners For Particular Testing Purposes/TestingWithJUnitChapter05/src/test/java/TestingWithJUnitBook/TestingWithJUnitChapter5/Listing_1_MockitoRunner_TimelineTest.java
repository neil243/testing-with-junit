package TestingWithJUnitBook.TestingWithJUnitChapter5;

import static org.junit.Assert.*;

import static TestingWithJUnitBook.TestingWithJUnitChapter5.ThrowableCaptor.thrownBy;
import static TestingWithJUnitBook.TestingWithJUnitChapter5.Timeline.ERROR_EXCEEDS_LOWER_BOUND;
import static TestingWithJUnitBook.TestingWithJUnitChapter5.Timeline.ERROR_EXCEEDS_UPPER_BOUND;
import static TestingWithJUnitBook.TestingWithJUnitChapter5.Timeline.FETCH_COUNT_LOWER_BOUND;
import static TestingWithJUnitBook.TestingWithJUnitChapter5.Timeline.FETCH_COUNT_UPPER_BOUND;
import static java.lang.String.format;
import static java.lang.String.valueOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith( MockitoJUnitRunner.class )
public class Listing_1_MockitoRunner_TimelineTest
{
	@Mock
	private SessionStorage sessionStorage;
	  
	private ItemProviderStub itemProvider;
	private Timeline timeline;
	  

	@Before
	public void setUp()
	{
	    itemProvider = new ItemProviderStub();
	    timeline = new Timeline( itemProvider, sessionStorage );
	}
	  
	@Test
	public void setFetchCountExceedsUpperBound() 
	{
	    int tooLarge = FETCH_COUNT_UPPER_BOUND + 1;
	      
	    Throwable actual = thrownBy( () -> timeline.setFetchCount( tooLarge ) );

	    assertNotNull( actual );  
	    assertTrue( actual instanceof IllegalArgumentException );
	    assertTrue( actual.getMessage().contains( valueOf( tooLarge ) ) );
	    assertEquals( format( ERROR_EXCEEDS_UPPER_BOUND, tooLarge ), 
	                  actual.getMessage() );
	}
	  
	@Test
	public void setFetchCountExceedsLowerBound() 
	{
	    int tooSmall = FETCH_COUNT_LOWER_BOUND - 1;
	      
	    Throwable actual = thrownBy( () -> timeline.setFetchCount( tooSmall ) );

	    assertNotNull( actual );  
	    assertTrue( actual instanceof IllegalArgumentException );
	    assertTrue( actual.getMessage().contains( valueOf( tooSmall ) ) );
	    assertEquals( format( ERROR_EXCEEDS_LOWER_BOUND, tooSmall ), 
	                  actual.getMessage() );
	}
}