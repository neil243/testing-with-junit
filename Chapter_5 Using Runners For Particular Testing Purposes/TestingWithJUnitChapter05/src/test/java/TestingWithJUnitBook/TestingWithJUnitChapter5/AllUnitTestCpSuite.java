package TestingWithJUnitBook.TestingWithJUnitChapter5;

import org.junit.runner.RunWith;
import org.junit.extensions.cpsuite.ClasspathSuite;
import org.junit.extensions.cpsuite.ClasspathSuite.ClassnameFilters;

@RunWith(ClasspathSuite.class)
@ClassnameFilters({
".*Test",
"!.*ITest"
})
public class AllUnitTestCpSuite {}
