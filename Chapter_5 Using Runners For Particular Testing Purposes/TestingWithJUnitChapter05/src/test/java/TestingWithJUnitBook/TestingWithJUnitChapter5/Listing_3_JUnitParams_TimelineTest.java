package TestingWithJUnitBook.TestingWithJUnitChapter5;

import static TestingWithJUnitBook.TestingWithJUnitChapter5.Timeline.FETCH_COUNT_UPPER_BOUND;
import static TestingWithJUnitBook.TestingWithJUnitChapter5.Timeline.FETCH_COUNT_LOWER_BOUND;

import static TestingWithJUnitBook.TestingWithJUnitChapter5.Timeline.ERROR_EXCEEDS_UPPER_BOUND;
import static TestingWithJUnitBook.TestingWithJUnitChapter5.Timeline.ERROR_EXCEEDS_LOWER_BOUND;

import static TestingWithJUnitBook.TestingWithJUnitChapter5.ThrowableCaptor.thrownBy;

import static java.lang.String.format;
import static java.lang.String.valueOf;

import static org.junit.Assert.*;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

import org.junit.runner.RunWith;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.mock;

@RunWith(JUnitParamsRunner.class)
public class Listing_3_JUnitParams_TimelineTest
{
	private ItemProvider itemProvider;
	private SessionStorage sessionStorage;
	private Timeline timeline;
	
	@Before
	public void setUp()
	{
		itemProvider = new ItemProviderStub();
		sessionStorage = mock(SessionStorage.class);
		timeline = new Timeline(itemProvider, sessionStorage);
	}
	
	@Test
	@Parameters (source = Listing_3_JUnitParams_FetchItemsDataProvider.class)
	public void fetchItems(FetchItemsData data)
	{
		((ItemProviderStub) itemProvider).addItems( data.getInput() );
	}
	
	@Test
	public void setFetchCountExceedsUpperBound()
	{
	    int tooLarge = FETCH_COUNT_UPPER_BOUND + 1;
	      
	    Throwable actual = thrownBy( () -> timeline.setFetchCount( tooLarge ) );

	    assertNotNull( actual );  
	    assertTrue( actual instanceof IllegalArgumentException );
	    assertTrue( actual.getMessage().contains( valueOf( tooLarge ) ) );
	    assertEquals( format( ERROR_EXCEEDS_UPPER_BOUND, tooLarge ), 
	                  actual.getMessage() );
	}
	
	@Test
	public void setFetchCountExceedsLowerBound() 
	{
	    int tooSmall = FETCH_COUNT_LOWER_BOUND - 1;
	    
	    Throwable actual = thrownBy( () -> timeline.setFetchCount( tooSmall ) );
	    
	    assertNotNull( actual );  
	    assertTrue( actual instanceof IllegalArgumentException );
	    assertTrue( actual.getMessage().contains( valueOf( tooSmall ) ) );
	    assertEquals( format( ERROR_EXCEEDS_LOWER_BOUND, tooSmall ), 
	                  actual.getMessage() );
	}
}
