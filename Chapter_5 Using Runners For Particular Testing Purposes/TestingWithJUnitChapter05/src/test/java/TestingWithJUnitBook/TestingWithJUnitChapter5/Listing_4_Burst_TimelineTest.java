package TestingWithJUnitBook.TestingWithJUnitChapter5;

import static TestingWithJUnitBook.TestingWithJUnitChapter5.Timeline.FETCH_COUNT_UPPER_BOUND;
import static TestingWithJUnitBook.TestingWithJUnitChapter5.Timeline.FETCH_COUNT_LOWER_BOUND;

import static TestingWithJUnitBook.TestingWithJUnitChapter5.Timeline.ERROR_EXCEEDS_UPPER_BOUND;
import static TestingWithJUnitBook.TestingWithJUnitChapter5.Timeline.ERROR_EXCEEDS_LOWER_BOUND;

import static TestingWithJUnitBook.TestingWithJUnitChapter5.ThrowableCaptor.thrownBy;

import java.util.List;

import static java.lang.String.format;
import static java.lang.String.valueOf;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.Before;

import org.junit.runner.RunWith;

import com.squareup.burst.BurstJUnit4;

import static org.mockito.Mockito.mock;

@RunWith(BurstJUnit4.class)
public class Listing_4_Burst_TimelineTest
{
	private ItemProvider itemProvider;
	private SessionStorage sessionStorage;
	private Timeline timeline;
	
	@Before
	public void setUp()
	{
		itemProvider = new ItemProviderStub();
		sessionStorage = mock( SessionStorage.class );
		timeline = new Timeline( itemProvider, sessionStorage );
	}
	
	@Test
	public void fetchItems(FetchItemsEnum data)
	{
		( ( ItemProviderStub ) itemProvider).addItems( data.getInput() );
		timeline.setFetchCount( data.getFetchCount() );
		timeline.fetchItems();
		
		timeline.fetchItems();
		List<Item> actual = timeline.getItems();
		
		assertArrayEquals( data.getOutput(), actual.toArray() );
	}
	
	@Test
	public void setFetchCountExceedsUpperBound()
	{
		int tooLarge = FETCH_COUNT_UPPER_BOUND + 1;
		
		Throwable actual = thrownBy( () -> timeline.setFetchCount( tooLarge ) );
		
		assertNotNull(actual);
		assertTrue(actual instanceof IllegalArgumentException);
		assertTrue( actual.getMessage().contains( valueOf( tooLarge ) ));
		assertEquals( format( ERROR_EXCEEDS_UPPER_BOUND, tooLarge ), actual.getMessage() );
	}
	
	@Test
	public void setFetchCountExceedsLowerBound()
	{
		int tooSmall = FETCH_COUNT_LOWER_BOUND - 1;
		
		Throwable actual = thrownBy( () -> timeline.setFetchCount( tooSmall) );
		
		assertNotNull( actual );
		assertTrue( actual instanceof IllegalArgumentException );
		assertTrue(actual.getMessage().contains( valueOf( tooSmall ) ) );
		assertEquals( format( ERROR_EXCEEDS_LOWER_BOUND, tooSmall ), actual.getMessage() );
	}
	
}
