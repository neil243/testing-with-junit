package TestingWithJUnitBook.TestingWithJUnitChapter5;

import org.junit.runner.RunWith;
import org.junit.runners.Suite.SuiteClasses;
import org.junit.experimental.categories.Categories;
import org.junit.experimental.categories.Categories.IncludeCategory;

@RunWith(Categories.class)
@IncludeCategory( Unit.class )
@SuiteClasses({
	AllTestSuite.class
})
public class AllUnitTestSuite {}
