import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class TimelineTest
{
	@Test
	public void setFetchCount()
	{
		Timeline timeline = new Timeline();
		int expected = 5;
		
		timeline.setFetchCount(expected);
		int actual = timeline.getFetchCount();
		
		assertEquals(expected, actual);
	}
}